﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombController : MonoBehaviour
{
    [Tooltip("Velocidad de caída de la bomba")]
    public float speed = 3;
    [Tooltip("Máxima Y permitida")]
    //public float yLimit = -1.5f;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        if (rb == null)
        {
            enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
       
        Vector3 currentPos = rb.position;
        currentPos.y -= speed * Time.deltaTime;
        rb.MovePosition(currentPos);
        //t.position = pos;
        //pos.y = pos.y - speed * Time.deltaTime;
        /*
        Transform t = GetComponent<Transform>();
        GameObject player;
        player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            Collider collider = player.GetComponent<Collider>();
            if (collider != null)
            {
                if (collider.bounds.Contains(transform.position))
                {
                    Catched();
                    return;
                }
            }
        }
        

        if (Grounded())
        {
            foreach (GameObject go in GameObject.FindGameObjectsWithTag("Bomb"))
            {
                BombController bc;
                bc = go.GetComponent<BombController>();
                if (bc != null)
                {
                    bc.Explode();
                }
            }
        }
        */
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Catched();
        }
    }
    /*
    bool Grounded()
    {
        return transform.position.y <= yLimit;
    }
    void Explode()
    {
        Destroy(this.gameObject);
    }
    */
    void Catched()
    {
        Destroy(this.gameObject);
    }

}

